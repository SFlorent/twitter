# Documentation d’installation - Projet Twitter - Florent SIMONET Gaëtan MERRIEN Sébastien FOUCHEUR

Ce projet utilise Node.js / Express pour la backend et Angular pour le frontend.

## Prérequis

- Avoir NodeJS installé

## Installation

### Cloner le projet Git

Cloner le projet git avec la commande : `git clone git@gitlab.com:SFlorent/twitter.git` ou `git clone https://gitlab.com/SFlorent/twitter.git`

### Installer les dépendances et lancer le projet

Lancer la commande `npm install -g nodemon` si vous n'avez pas nodemon d'installé

#### Script init.sh

A la racine du projet :

Lancer la commande `./init.sh`

#### Si le script init.sh ne marche pas

Dans le dossier backend : 

Lancer la commande `npm install` puis lancer la commande `npm start`

Dans le dossier frontend : 

Lancer la commande `npm install` puis lancer la commande `npm start`


### Accès au projet

http://localhost:4200 






