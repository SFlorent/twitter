import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    if (document.querySelector('.sidebar-navigation ul li.active')) {
      document.querySelector('.sidebar-navigation ul li.active').classList.remove('active');
    }
    document.querySelector('#homeNav').classList.add('active');
  }

}
