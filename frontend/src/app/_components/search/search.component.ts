import { Component, OnInit } from '@angular/core';
import { TweetsService } from 'src/app/_services/tweets.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  model: { keyWord: string, tweetsCount: number };
  tweets: object;

  constructor(private tweetsService: TweetsService) {
    this.model = { keyWord: "", tweetsCount: 30 };
  }

  ngOnInit() {
    if (document.querySelector('.sidebar-navigation ul li.active')) {
      document.querySelector('.sidebar-navigation ul li.active').classList.remove('active');
    }
    document.querySelector('#searchNav').classList.add('active');
    document.getElementById("preloader").style.display = "none";
  }

  search_tweets() {

    var ok = true;

    var title;
    var text;
    if (localStorage.getItem("language") == "fr") {
      title = "Erreur !";
    } else if (localStorage.getItem("language") == "en") {
      title = "Error !";
    } else if (localStorage.getItem("language") == "es") {
      title = "Error !";
    }

    if (this.model.keyWord == "") {
      if (this.model.tweetsCount > 100 || this.model.tweetsCount < 1) {
        if (localStorage.getItem("language") == "fr") {
          text = "Veuillez renseigner un mot-clé et un nombre de tweets valide.";
        } else if (localStorage.getItem("language") == "en") {
          text = "Please enter a valid keyword and a valid number of tweets.";
        } else if (localStorage.getItem("language") == "es") {
          text = "Por favor, introduzca una palabra clave válido y un número válido de tweets.";
        }
        Swal.fire(
          title,
          text,
          'error'
        );
      } else {
        if (localStorage.getItem("language") == "fr") {
          text = "Veuillez renseigner un mot-clé valide";
        } else if (localStorage.getItem("language") == "en") {
          text = "Please enter a valid keyword.";
        } else if (localStorage.getItem("language") == "es") {
          text = "Por favor, introduzca una palabra clave válido.";
        }
        Swal.fire(
          title,
          text,
          'error'
        );
      }
      ok = false;
    } else {
      if (localStorage.getItem("language") == "fr") {
        text = "Veuillez renseigner un nombre de tweets valide.";
      } else if (localStorage.getItem("language") == "en") {
        text = "Please enter a valid number of tweets.";
      } else if (localStorage.getItem("language") == "es") {
        text = "Por favor, introduzca un número válido de tweets.";
      }
      if (this.model.tweetsCount > 100 || this.model.tweetsCount < 1) {
        Swal.fire(
          title,
          text,
          'error'
        );
        ok = false;
      }
    }


    if (ok) {
      this.tweets = [];
      document.getElementById("preloader").style.display = "";
      this.tweetsService.search(this.model.keyWord, this.model.tweetsCount).then((data: any) => {
        this.tweets = data.tweets.statuses;
        document.getElementById("preloader").style.display = "none";
      });
    }
  }

}
