import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tweet-card',
  templateUrl: './tweet-card.component.html',
  styleUrls: ['./tweet-card.component.scss']
})
export class TweetCardComponent implements OnInit {

  @Input() tweet: any;

  constructor() { 
  }

  ngOnInit() {
    let tmp = this.tweet.text;
    this.tweet.text = this.tweet.text.split("https://")[0];
    this.tweet.tweetLink = tmp.split("https://")[1];

    console.log(this.tweet);
  }

}
