import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  model: { language: string };

  constructor(private translate: TranslateService) {
    this.model = { language: localStorage.getItem("language") };
  }

  ngOnInit() {
    if (document.querySelector('.sidebar-navigation ul li.active')) {
      document.querySelector('.sidebar-navigation ul li.active').classList.remove('active');
    }
    document.querySelector('#settingsNav').classList.add('active');
  }

  change_language() {

    localStorage.setItem("language", this.model.language);
    this.translate.use(this.model.language);

    var title;
    var text;
    if (localStorage.getItem("language") == "fr") {
      title = "Mis à jour !";
      text = "Les paramètres ont été mis à jour.";
    } else if (localStorage.getItem("language") == "en") {
      title = "Updated!";
      text = "The settings have been updated.";
    } else if (localStorage.getItem("language") == "es") {
      title = "¡Actualizado!";
      text = "Los ajustes han sido actualizados.";
    }
    Swal.fire(
      title,
      text,
      'success'
    );
  }

}
