import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './_components/home/home.component';
import { MainComponent } from './_components/main/main.component';
import { SettingsComponent } from './_components/settings/settings.component';
import { SearchComponent } from './_components/search/search.component';


const routes: Routes = [
  {path: '', component: MainComponent, children: [
    { path: '', component: HomeComponent },
    { path: 'search', component: SearchComponent },
    { path: 'settings', component: SettingsComponent }
  ]},
  {path: '**', pathMatch: 'full', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
