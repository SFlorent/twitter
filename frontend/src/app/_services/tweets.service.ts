import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TweetsService {

  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*"
    })
  };

  constructor(private http: HttpClient) {}

  search(q, count){
    return this.http.get( environment.api_url + '/search_tweets/' + q + '/' + count + '/' + localStorage.getItem('language'), this.httpOptions).toPromise();
  } 


}
