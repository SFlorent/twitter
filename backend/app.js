/**
 * Module dependencies.
 */
var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var logger = require('morgan');
var helmet = require('helmet');
var cors = require('cors');
require('dotenv').config();

var indexRouter = require('./routes/index');

var app = express();

var whitelist = [process.env.Front_url]
var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS'))
    }
  }
}

app.use(cors(corsOptions));

app.use(session({
  secret: 'YJ3qw$XCTpRb',
  resave: false,
  saveUninitialized: false,
  cookie: {
    maxAge: 86400000,
    httpOnly: false,
    secure: true
  }
}));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(bodyParser.json());
app.use(helmet());

app.use('/api', indexRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  res.status(404);
  res.send("/search_tweets/:q/:count/:language : Cherche des tweets selon un mot clé");
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.send(res.locals.message);
});

module.exports = app;