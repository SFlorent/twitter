var express = require('express');
var router = express.Router();
var request = require("request");

var Twitter = require('twitter');

var client = new Twitter({
  consumer_key: process.env.API_key,
  consumer_secret: process.env.API_secret_key,
  access_token_key: process.env.Access_token,
  access_token_secret: process.env.Access_token_secret
});


/* GET search tweets. */
router.get('/search_tweets/:q/:count/:language', function (req, res, next) {
  client.get('search/tweets', { q: req.params.q, lang: req.params.language , count: req.params.count, result_type: 'mixed', include_entities: 'true'}, function (error, tweets, response) {
    res.send({
      tweets: tweets
    });
  });
});




module.exports = router;